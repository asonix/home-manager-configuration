{ config, pkgs, ... }:

let
  server = ({ hostname, user ? "asonix", port ? 22, proxyJump ? true }: {
    hostname = hostname;
    user = user;
    identitiesOnly = true;
    identityFile = "/home/asonix/.ssh/kube-rsa";
    port = port;
    proxyJump = if proxyJump then "router" else null;
  });
in
{
  programs.ssh = {
    enable = true;

    matchBlocks = {
      "codeberg.org" = {
        hostname = "codeberg.org";
        user = "git";
        identitiesOnly = true;
        identityFile = "/home/asonix/.ssh/codeberg";
        port = 22;
      };
      "firestar" = {
        hostname = "192.168.5.10";
        user = "asonix";
        identitiesOnly = true;
        identityFile = "/home/asonix/.ssh/firestar";
        port = 22;
        proxyJump = "router";
      };
      "github.com" = {
        hostname = "github.com";
        user = "git";
        identitiesOnly = true;
        identityFile = "/home/asonix/.ssh/github";
        port = 22;
      };
      "gitlab.com" = {
        hostname = "gitlab.com";
        user = "git";
        identitiesOnly = true;
        identityFile = "/home/asonix/.ssh/gitlab";
        port = 22;
      };
      "git.asonix.dog" = {
        hostname = "git.asonix.dog";
        user = "git";
        identitiesOnly = true;
        identityFile = "/home/asonix/.ssh/gitea-key";
        port = 22;
      };
      "git.deuxfleurs.fr" = {
        hostname = "git.deuxfleurs.fr";
        user = "git";
        identitiesOnly = true;
        identityFile = "/home/asonix/.ssh/garage-git";
        port = 22;
      };
      "router" = server {
        hostname = "ssh.asonix.dog";
        port = 3128;
        proxyJump = false;
      };
      "lionheart" = server { hostname = "192.168.5.6"; };
      "pinetab2" = server { hostname = "192.168.5.13"; };
      "redtail" = server { hostname = "192.168.20.23"; };
      "redtail2" = server { hostname = "192.168.20.24"; };
      "whitestorm" = server { hostname = "192.168.20.26"; };
      "whitestorm2" = server { hostname = "192.168.20.27"; };
      "build2" = server { hostname = "192.168.20.101"; };
      "bluestar" = server { hostname = "192.168.20.109"; };
      "k3s-rock1" = server { hostname = "192.168.20.110"; };
      "k3s-rock2" = server { hostname = "192.168.20.111"; };
      "k3s-rock3" = server { hostname = "192.168.20.112"; };
      "k3s-rock4" = server { hostname = "192.168.20.113"; };
      "k3s-rock5" = server { hostname = "192.168.20.114"; };
      "k3s-rock6" = server { hostname = "192.168.20.115"; };
      "k3s-rock7" = server { hostname = "192.168.20.116"; };
      "k3s1" = server { hostname = "192.168.20.120"; };
      "k3s2" = server { hostname = "192.168.20.121"; };
      "k3s3" = server { hostname = "192.168.20.122"; };
      "k3s4" = server { hostname = "192.168.20.123"; };
      "k3s5" = server { hostname = "192.168.20.124"; };
      "k3s6" = server { hostname = "192.168.20.125"; };
      "k3s7" = server { hostname = "192.168.20.126"; };
      "k3s8" = server { hostname = "192.168.20.127"; };
      "k3s9" = server { hostname = "192.168.20.128"; };
      "k3s10" = server { hostname = "192.168.20.129"; };
      "k3s11" = server { hostname = "192.168.20.130"; };
      "k3s12" = server { hostname = "192.168.20.131"; };
      "k3s13" = server { hostname = "192.168.20.132"; };
      "k3s14" = server { hostname = "192.168.20.133"; };
      "k3s15" = server { hostname = "192.168.20.134"; };
      "k3s16" = server { hostname = "192.168.20.135"; };
      "k3s-quartza1" = server { hostname = "192.168.20.160"; };
      "backup1" = server { hostname = "192.168.20.190"; };
      "backup2" = server { hostname = "192.168.20.191"; };
      "octoprint" = server { hostname = "192.168.20.32"; user = "pi"; };
      "jellyfin" = server { hostname = "192.168.20.195"; };
    };
  };
}
