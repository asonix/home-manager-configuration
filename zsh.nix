{ config, pkgs, ... }:

{
  programs.zsh = {
    enable = true;
    dotDir = ".config/zsh";
    autosuggestion.enable = true;
    enableCompletion = true;
    autocd = true;
    defaultKeymap = "viins";
    shellAliases = {
      cat = "bat";
      l = "eza -l";
      la = "eza -la";
      ls = "eza";
      v = "lvim";
    };
    syntaxHighlighting.enable = true;

    initExtra = ''
      source "${pkgs.zsh-nix-shell}/share/zsh-nix-shell/nix-shell.plugin.zsh"
      source "${pkgs.zsh-z}/share/zsh-z/zsh-z.plugin.zsh"

      PATH=$PATH:$HOME/.local/bin
    '';

    plugins = with pkgs; [
      {
        name = "base16-shell";
        src = fetchFromGitHub {
          owner = "chriskempson";
          repo = "base16-shell";
          rev = "ae84047d378700bfdbabf0886c1fb5bb1033620f";
          sha256 = "0qy+huAbPypEMkMumDtzcJdQQx5MVgsvgYu4Em/FGpQ=";
        };
        file = "base16-shell.plugin.zsh";
      }
    ];
  };
}
