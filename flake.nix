{
  description = "Home Manager configuration of Jane Doe";

  inputs = {
    # Specify the source of Home Manager and Nixpkgs.
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    # obs-streamfx = {
    #   url = "git+https://git.asonix.dog/asonix/nix-obs-streamfx";
    #   inputs.nixpkgs.follows = "nixpkgs";
    # };
    niri-flake = {
      url = "git+https://git.asonix.dog/asonix/niri-flake?ref=asonix/pantheon-stuff";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { nixpkgs, home-manager, niri-flake, ... }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs {
        inherit system;
        config.allowUnfree = true;

        overlays = [
          (final: prev: {
            obs-studio-plugins = prev.obs-studio-plugins // {
              # obs-streamfx = obs-streamfx.packages.${system}.obs-streamfx;
            };
          })
          niri-flake.overlays.niri
        ];
      };
    in
    {
      homeConfigurations.asonix = home-manager.lib.homeManagerConfiguration {
        inherit pkgs;

        # Specify your home configuration modules here, for example,
        # the path to your home.nix.
        modules = [
          niri-flake.homeModules.niri
          ./home.nix
          ./nvim.nix
          ./niri.nix
        ];

        # Optionally use extraSpecialArgs
        # to pass through arguments to home.nix
        # extraSpecialArgs = attrs // { inherit system; };
      };
    };
}
