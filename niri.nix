{ config, pkgs, ... }:

{
  services.gnome-keyring.enable = true;
  services.ssh-agent.enable = true;
  services.mako = {
    enable = true;
    defaultTimeout = 10000;
    font = "FantasqueSansM Nerd Font Mono";
    padding = "16";
    backgroundColor = "#970542";
    borderColor = "#e10862";
    borderRadius = 8;
    borderSize = 4;
  };

  systemd.user.services.laptop-bg = {
    Unit = {
      description = "Background for laptop";
      After = ["graphical-session.target"];
      PartOf = ["graphical-session.target"];
    };
    Install = {
      WantedBy = ["graphical-session.target"];
    };
    Service = {
      Type = "simple";
      ExecStart = "${pkgs.swaybg}/bin/swaybg -o eDP-1 -m fill -i %h/Pictures/furry/Tavi/WarmPawsArt/6677676.png";
      Restart = "on-failure";
      RestartSec = 1;
      TimeoutStopSec = 10;
    };
  };

  systemd.user.services.desktop-bg = {
    Unit = {
      description = "Background for laptop";
      After = ["graphical-session.target"];
      PartOf = ["graphical-session.target"];
    };
    Install = {
      WantedBy = ["graphical-session.target"];
    };
    Service = {
      Type = "simple";
      ExecStart = "${pkgs.swaybg}/bin/swaybg -o DP-2 -m fill -i '%h/Pictures/furry/commissions/6my/asonix shenzi thoughts.png' -o DP-3 -m fill -i %h/Pictures/furry/edits/Tavi/WarmPawsArt/6677676-left.png -o DP-1 -m fill -i %h/Pictures/furry/edits/Tavi/WarmPawsArt/6677676-right.png";
      Restart = "on-failure";
      RestartSec = 1;
      TimeoutStopSec = 10;
    };
  };

  gtk = {
    enable = true;
    cursorTheme = {
      package = pkgs.pantheon.elementary-icon-theme;
      name = "elementary";
    };
    iconTheme = {
      name = "elementary";
      package = pkgs.pantheon.elementary-icon-theme;
    };
    theme.name = "io.elementary.stylesheet.bubblegum";
  };

  programs.swaylock.settings = {
    color = "#222222";
  };

  programs.alacritty = {
    enable = true;
    settings = {
      window = {
        blur = true;
        opacity = 0.95;
        padding = {
          x = 8;
          y = 8;
        };
      };
      font = {
        normal = {
          family = "FantasqueSansM Nerd Font Mono";
          style = "Regular";
        };
        bold = {
          family = "FantasqueSansM Nerd Font Mono";
          style = "Bold";
        };
        italic = {
          family = "FantasqueSansM Nerd Font Mono";
          style = "Italic";
        };
        bold_italic = {
          family = "FantasqueSansM Nerd Font Mono";
          style = "Bold Italic";
        };
      };
    };
  };

  programs.waybar = {
    enable = true;
    style = ./style.css;
    settings.mainBar = {
      layer = "top";
      height = 30;
      modules-left = [
        "niri/workspaces"
        "backlight"
        "pulseaudio"
        "custom/waybar-mpris"
      ];
      modules-center = [
        "clock"
      ];
      modules-right = [
        "network"
        "power-profiles-daemon"
        "cpu"
        "memory"
        "temperature"
        "keyboard-state"
        "battery"
        "tray"
      ];

      "niri/workspaces" = {
          format = "{icon}";
          format-icons = {
              active = "<big></big>";
              default = "<small></small>";
          };
      };

      "custom/waybar-mpris" = {
          return-type = "json";
          exec = "${pkgs.waybar-mpris}/bin/waybar-mpris --position --autofocus";
          on-click = "${pkgs.waybar-mpris}/bin/waybar-mpris --send toggle";
          # This option will switch between players on right click.
          on-click-right = "${pkgs.waybar-mpris}/bin/waybar-mpris --send player-next";
          # The options below will switch the selected player on scroll
              # "on-scroll-up": "waybar-mpris --send player-next",
              # "on-scroll-down": "waybar-mpris --send player-prev",
          # The options below will go to next/previous track on scroll
              # "on-scroll-up": "waybar-mpris --send next",
              # "on-scroll-down": "waybar-mpris --send prev",
          escape = true;
      };

      pulseaudio = {
        format = "{volume}% {icon} {format_source}";
        format-bluetooth = "{volume}% {icon} {format_source}";
        format-bluetooth-muted = " {icon} {format_source}";
        format-muted = " {format_source}";
        format-source = "{volume}% ";
        format-source-muted = "";
        format-icons = {
            headphone = "";
            hands-free = "";
            headset = "";
            phone = "";
            portable = "";
            car = "";
            default = ["" "" ""];
        };
        on-click = "pavucontrol";
      };

      network = {
        format-wifi = "{essid} ({signalStrength}%) ";
        format-ethernet = "{ipaddr}/{cidr} ";
        tooltip-format = "{ifname} via {gwaddr} ";
        format-linked = "{ifname} (No IP) ";
        format-disconnected = "Disconnected ⚠";
        format-alt = "{ifname}: {ipaddr}/{cidr}";
      };

      power-profiles-daemon = {
        format = "{icon}";
        tooltip-format = "Power profile: {profile}\nDriver: {driver}";
        tooltip = true;
        format-icons = {
          default = "";
          performance = "";
          balanced = "";
          power-saver = "";
        };
      };

      cpu = {
          format = "{usage}% ";
          tooltip = false;
      };

      memory = {
          format = "{}% ";
      };

      temperature = {
          critical-threshold = 80;
          format = "{temperatureC}°C {icon}";
          format-icons = ["" "" ""];
      };

      backlight = {
        format = "{percent}% {icon}";
        format-icons = ["" "" "" "" "" "" "" "" ""];
        on-scroll-up = "${pkgs.brightnessctl}/bin/brightnessctl -c backlight set +1";
        on-scroll-down = "${pkgs.brightnessctl}/bin/brightnessctl -c backlight set 1-";
      };

      keyboard-state = {
          numlock = true;
          capslock = true;
          format = "{name} {icon}";
          format-icons = {
              locked = "";
              unlocked = "";
          };
      };

      battery = {
          states = {
              warning = 30;
              critical = 15;
          };
          format = "{capacity}% {icon}";
          format-full = "{capacity}% {icon}";
          format-charging = "{capacity}% ";
          format-plugged = "{capacity}% ";
          format-alt = "{time} {icon}";
          format-icons = ["" "" "" "" ""];
      };

      clock = {
          format = "{:%a %d %b %H:%M}";
          format-alt = "Week {:%V of %Y}";
      };

      tray = {
          spacing = 10;
      };
    };

    systemd.enable = true;
  };

  programs.niri = {
    package = pkgs.niri-stable;
    settings = {
      environment = {
        DISPLAY = ":0";
        ELECTRON_OZONE_PLATFORM_HINT = "wayland";
      };
      spawn-at-startup = [
        { command = ["${pkgs.xwayland-satellite-unstable}/bin/xwayland-satellite"]; }
        { command = ["${pkgs.wlsunset}/bin/wlsunset" "-l" "30.2" "-L" "-97.7"]; }
        { command = ["systemctl" "--user" "reset-failed" "waybar.service"]; }
        { command = ["systemctl" "--user" "reset-failed" "app-com.nextcloud.desktopclient.nextcloud@autostart.service"]; }
        { command = ["systemctl" "--user" "reset-failed" "app-org.kde.kdeconnect.daemon@autostart.service"]; }
        { command = ["systemctl" "--user" "reset-failed" "xdg-desktop-portal-gtk.service"]; }
        { command = ["systemctl" "--user" "reset-failed" "app-Nextcloud@autostart.service"]; }
      ];
      screenshot-path = "~/Pictures/Screenshots/Screenshot from %Y-%m-%d %H-%M-%S.png";
      prefer-no-csd = true;
      layout = {
        gaps = 16;
        focus-ring.enable = false;
      };
      window-rules = [{
        geometry-corner-radius = {
          top-left = 12.0;
          top-right = 12.0;
          bottom-right = 12.0;
          bottom-left = 12.0;
        };
        clip-to-geometry = true;
        border = {
          enable = true;
          active.color = "#c80757";
        };
      } {
        matches = [{
          app-id = "^xdg-desktop-portal-pantheon$";
          title = "^xdg-desktop-portal-pantheon$";
        }];
        open-floating = true;
      }];
      cursor.theme = "elementary";
      input.mouse.natural-scroll = true;
      outputs = {
        "DP-2" = {
          mode = {
            width = 1920;
            height = 1080;
            refresh = 143.855;
          };
          position.x = 1234;
          position.y = 1114;
          scale = 0.75;
        };
        "DP-3" = {
          position.x = 0;
          position.y = 0;
          transform.rotation = 90;
        };
        "DP-1" = {
          position.x = 3154;
          position.y = 0;
          transform.rotation = 270;
        };
      };
      binds = with config.lib.niri.actions; {
        "Mod+T".action.spawn = "${pkgs.alacritty}/bin/alacritty";
        "Mod+D".action.spawn = "${pkgs.fuzzel}/bin/fuzzel";
        "Super+Alt+L".action.spawn = "${pkgs.swaylock}/bin/swaylock";

        "XF86AudioRaiseVolume" = {
          allow-when-locked = true;
          action.spawn = ["${pkgs.wireplumber}/bin/wpctl" "set-volume" "@DEFAULT_AUDIO_SINK@" "0.1+"];
        };
        "XF86AudioLowerVolume" = {
          allow-when-locked = true;
          action.spawn = ["${pkgs.wireplumber}/bin/wpctl" "set-volume" "@DEFAULT_AUDIO_SINK@" "0.1-"];
        };
        "XF86AudioMute" = {
          allow-when-locked = true;
          action.spawn = ["${pkgs.wireplumber}/bin/wpctl" "set-mute" "@DEFAULT_AUDIO_SINK@" "toggle"];
        };
        "XF86AudioMicMute" = {
          allow-when-locked = true;
          action.spawn = ["${pkgs.wireplumber}/bin/wpctl" "set-mute" "@DEFAULT_AUDIO_SOURCE@" "toggle"];
        };
        "XF86MonBrightnessDown" = {
          allow-when-locked = true;
          action.spawn = ["${pkgs.brightnessctl}/bin/brightnessctl" "-c" "backlight" "set" "1-"];
        };
        "XF86MonBrightnessUp" = {
          allow-when-locked = true;
          action.spawn = ["${pkgs.brightnessctl}/bin/brightnessctl" "-c" "backlight" "set" "+1"];
        };

        "Mod+Shift+Slash".action = show-hotkey-overlay;

        "Mod+Q".action = close-window;

        "Mod+Left".action = focus-column-left;
        "Mod+Down".action = focus-window-down;
        "Mod+Up".action = focus-window-up;
        "Mod+Right".action = focus-column-right;
        "Mod+H".action = focus-column-left;
        "Mod+J".action = focus-window-down;
        "Mod+K".action = focus-window-up;
        "Mod+L".action = focus-column-right;

        "Mod+Ctrl+Left".action = move-column-left;
        "Mod+Ctrl+Down".action = move-window-down;
        "Mod+Ctrl+Up".action = move-window-up;
        "Mod+Ctrl+Right".action = move-column-right;
        "Mod+Ctrl+H".action = move-column-left;
        "Mod+Ctrl+J".action = move-window-down;
        "Mod+Ctrl+K".action = move-window-up;
        "Mod+Ctrl+L".action = move-column-right;

        "Mod+Home".action = focus-column-first;
        "Mod+End".action = focus-column-last;
        "Mod+Ctrl+Home".action = move-column-to-first;
        "Mod+Ctrl+End".action = move-column-to-last;

        "Mod+Shift+Left".action = focus-monitor-left;
        "Mod+Shift+Down".action = focus-monitor-down;
        "Mod+Shift+Up".action = focus-monitor-up;
        "Mod+Shift+Right".action = focus-monitor-right;
        "Mod+Shift+H".action = focus-monitor-left;
        "Mod+Shift+J".action = focus-monitor-down;
        "Mod+Shift+K".action = focus-monitor-up;
        "Mod+Shift+L".action = focus-monitor-right;

        "Mod+Shift+Ctrl+Left".action = move-column-to-monitor-left;
        "Mod+Shift+Ctrl+Down".action = move-column-to-monitor-down;
        "Mod+Shift+Ctrl+Up".action = move-column-to-monitor-up;
        "Mod+Shift+Ctrl+Right".action = move-column-to-monitor-right;
        "Mod+Shift+Ctrl+H".action = move-column-to-monitor-left;
        "Mod+Shift+Ctrl+J".action = move-column-to-monitor-down;
        "Mod+Shift+Ctrl+K".action = move-column-to-monitor-up;
        "Mod+Shift+Ctrl+L".action = move-column-to-monitor-right;

        "Mod+Page_Down".action = focus-workspace-down;
        "Mod+Page_Up".action = focus-workspace-up;
        "Mod+U".action = focus-workspace-down;
        "Mod+I".action = focus-workspace-up;
        "Mod+Ctrl+Page_Down".action = move-column-to-workspace-down;
        "Mod+Ctrl+Page_Up".action = move-column-to-workspace-up;
        "Mod+Ctrl+U".action = move-column-to-workspace-down;
        "Mod+Ctrl+I".action = move-column-to-workspace-up;

        "Mod+Shift+Page_Down".action = move-workspace-down;
        "Mod+Shift+Page_Up".action = move-workspace-up;
        "Mod+Shift+U".action = move-workspace-down;
        "Mod+Shift+I".action = move-workspace-up;

        "Mod+WheelScrollDown" = {
          action = focus-workspace-down;
          cooldown-ms = 150;
        };
        "Mod+WheelScrollUp" = {
          action = focus-workspace-up;
          cooldown-ms = 150;
        };
        "Mod+Ctrl+WheelScrollDown" = {
          action = move-column-to-workspace-down;
          cooldown-ms = 150;
        };
        "Mod+Ctrl+WheelScrollUp" = {
          action = move-column-to-workspace-up;
          cooldown-ms = 150;
        };

        "Mod+WheelScrollRight".action = focus-column-right;
        "Mod+WheelScrollLeft".action = focus-column-left;
        "Mod+Ctrl+WheelScrollRight".action = move-column-right;
        "Mod+Ctrl+WheelScrollLeft".action = move-column-left;

        "Mod+Shift+WheelScrollDown".action = focus-column-right;
        "Mod+Shift+WheelScrollUp".action = focus-column-left;
        "Mod+Ctrl+Shift+WheelScrollDown".action = move-column-right;
        "Mod+Ctrl+Shift+WheelScrollUp".action = move-column-left;

        "Mod+1".action = focus-workspace 1;
        "Mod+2".action = focus-workspace 2;
        "Mod+3".action = focus-workspace 3;
        "Mod+4".action = focus-workspace 4;
        "Mod+5".action = focus-workspace 5;
        "Mod+6".action = focus-workspace 6;
        "Mod+7".action = focus-workspace 7;
        "Mod+8".action = focus-workspace 8;
        "Mod+9".action = focus-workspace 9;
        "Mod+Ctrl+1".action = move-column-to-workspace 1;
        "Mod+Ctrl+2".action = move-column-to-workspace 2;
        "Mod+Ctrl+3".action = move-column-to-workspace 3;
        "Mod+Ctrl+4".action = move-column-to-workspace 4;
        "Mod+Ctrl+5".action = move-column-to-workspace 5;
        "Mod+Ctrl+6".action = move-column-to-workspace 6;
        "Mod+Ctrl+7".action = move-column-to-workspace 7;
        "Mod+Ctrl+8".action = move-column-to-workspace 8;
        "Mod+Ctrl+9".action = move-column-to-workspace 9;

        "Mod+Comma".action = consume-window-into-column;
        "Mod+Period".action = expel-window-from-column;

        "Mod+BracketLeft".action = consume-or-expel-window-left;
        "Mod+BracketRight".action = consume-or-expel-window-right;

        "Mod+R".action = switch-preset-column-width;
        "Mod+Shift+R".action = switch-preset-window-height;
        "Mod+Ctrl+R".action = reset-window-height;
        "Mod+F".action = maximize-column;
        "Mod+Shift+F".action = fullscreen-window;
        "Mod+C".action = center-column;

        "Mod+V".action = toggle-window-floating;
        "Mod+Shift+V".action = switch-focus-between-floating-and-tiling;

        "Mod+Minus".action = set-column-width "-10%";
        "Mod+Equal".action = set-column-width "+10%";

        "Mod+Shift+Minus".action = set-window-height "-10%";
        "Mod+Shift+Equal".action = set-window-height "+10%";

        "Print".action = screenshot;
        "Ctrl+Print".action = screenshot-screen;
        "Alt+Print".action = screenshot-window;

        "Mod+Shift+E".action = quit;
        "Ctrl+Alt+Delete".action = quit;

        "Mod+Shift+P".action = power-off-monitors;
      };
    };
  };
}
