{ pkgs, lib, ... }:

{
  home.packages = with pkgs; [
    code-minimap
    fd
    gcc
    gnumake
    (lunarvim.overrideAttrs (finalAttrs: {
      installPhase = ''
        runHook preInstall

        mkdir -p $out
        cp -r bin share $out

        for iconDir in utils/desktop/*/; do
          install -Dm444 $iconDir/lvim.svg -t $out/share/icons/hicolor/$(basename $iconDir)/apps
        done

        install -Dm444 utils/desktop/lvim.desktop -t $out/share/applications

        wrapProgram $out/bin/lvim --prefix PATH : ${ lib.makeBinPath finalAttrs.runtimeDeps } \
          --prefix LD_LIBRARY_PATH : ${stdenv.cc.cc.lib} \
          --set CC ${stdenv.cc.targetPrefix}cc
      '' + lib.optionalString finalAttrs.nvimAlias ''
        ln -s $out/bin/lvim $out/bin/nvim
      '' + lib.optionalString finalAttrs.viAlias ''
        ln -s $out/bin/lvim $out/bin/vi
      '' + lib.optionalString finalAttrs.vimAlias ''
        ln -s $out/bin/lvim $out/bin/vim
      '' + ''
        runHook postInstall
      '';
    }))
    nodejs
    python310Full
    python310Packages.pynvim
    ripgrep
    spacevim
    tree-sitter
    wl-clipboard
    xclip
  ];
}
