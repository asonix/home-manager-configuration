{ config, pkgs, system, ... }:

let
  setIcon = ''
#!/usr/bin/env bash

dbus-send \
    --system \
    --print-reply \
    --type=method_call \
    --dest="org.freedesktop.Accounts" \
    /org/freedesktop/Accounts/User1000 \
    org.freedesktop.Accounts.User.SetIconFile \
    "string:$1"
  '';
in
{
  imports = [ ./zsh.nix ./ssh.nix ];

  home.username = "asonix";
  home.homeDirectory = "/home/asonix";

  home.file."lunarvim-config" = {
    enable = true;
    source = ./config.lua;
    target = ".config/lvim/config.lua";
  };

  home.file."rio-config" = {
    enable = true;
    source = ./rio-config.toml;
    target = ".config/rio/config.toml";
  };

  home.file."rio-theme" = {
    enable = true;
    source = ./base16-eighties.toml;
    target = ".config/rio/themes/base16-eighties.toml";
  };

  home.file."spacevim-config" = {
    enable = true;
    source = ./init.toml;
    target = ".SpaceVim.d/init.toml";
  };

  home.packages = with pkgs; [
    ack
    bat
    bottom
    dig
    eza
    file
    htop
    hyfetch
    topgrade
    keymapp

    amberol
    # carla
    # cura
    dconf-editor
    # dissent
    easyeffects
    # fluffychat
    fractal
    freecad
    gimp-with-plugins
    handbrake
    helvum
    krita
    libreoffice-fresh
    mission-center
    newsflash
    nextcloud-client
    paper-plane
    patchage
    pavucontrol
    prismlauncher
    shticker-book-unwritten
    signal-desktop
    simple-scan
    tdesktop
    tuba
    ungoogled-chromium
    vesktop
    vlc
    wireshark

    picocom

    (writeShellScriptBin "set-icon" setIcon)
  ];

  home.stateVersion = "22.05";

  programs.home-manager.enable = true;
  programs.git = {
    enable = true;
    userName = "asonix";
    userEmail = "asonix@asonix.dog";
  };

  programs.bat.enable = true;

  programs.starship.enable = true;

  programs.direnv = {
    enable = true;
    nix-direnv.enable = true;
  };

  programs.obs-studio = {
    enable = true;
    plugins = with pkgs.obs-studio-plugins; [
      # advanced-scene-switcher
      # droidcam-obs
      input-overlay
      obs-3d-effect
      obs-backgroundremoval
      obs-command-source
      obs-freeze-filter
      obs-gradient-source
      obs-gstreamer
      obs-move-transition
      obs-multi-rtmp
      obs-mute-filter
      obs-pipewire-audio-capture
      # obs-replay-source
      obs-rgb-levels-filter
      obs-scale-to-sound
      obs-shaderfilter
      obs-source-record
      obs-source-switcher
      # obs-streamfx
      obs-teleport
      obs-text-pthread
      # obs-transition-table
      obs-tuna
      obs-vaapi
      # obs-vertical-canvas
      obs-vintage-filter
      obs-vkcapture
      waveform
      wlrobs
    ];
  };
}
